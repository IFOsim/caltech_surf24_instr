# Gravitational wave detection with laser interferometers
## 2024 Caltech SURF Program - Lecture 2

This git repository is a companion to the 2024 Caltech SURF Program.

Before the lesson, make sure to install this package and run the first cell block in the Jupyter Notebook `lesson2.ipynb`.

The companion slides can be found at [LIGO Document Number G2401359](https://dcc.ligo.org/G2401359).

## Installation
You will need two packages: Finesse and GWINC. Everything else is installed as a dependency.

If you are installing using Conda, follow these instructions. Otherwise, please see the [Finesse installation](https://finesse.ifosim.org/docs/latest/getting_started/install/first.html#installing-finesse-and-jupyter-with-conda) page for more information.

Step 1 - Update Conda
```
conda update -n base conda
```

Step 2 - Create an environment for today's lesson
```
conda create -n surf24 python=3.10
conda activate surf24
```

Step 3 - Install the Finesse, GWINC, and Jupyter packages from conda-forge
```
conda install -c conda-forge finesse jupyter gwinc
```

Now run `jupyter notebook`, open `lesson2.ipynb`, and execute the first cell. If you encounter any errors, contact awjones@caltech.edu or aaron.jones on Mattermost.

We won't have time to fix errors in the lesson!