{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "459c6b4e-6e51-4b0f-8379-1f9fc1d62607",
   "metadata": {},
   "source": [
    "# Intro to GWD\n",
    "\n",
    "This notebook is a companion to the 2024 Caltech SURF lecture series - _Gravitational wave detection with laser interferometers_.\n",
    "\n",
    "You can find the slides on the DCC, [LIGO Document Number: G2401359](https://dcc.ligo.org/G2401359)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a2d17946-7ac1-4ff8-a880-58de5fab586b",
   "metadata": {},
   "source": [
    "## Cell Block 0\n",
    "Run this before the class. If you see any errors, email awjones@caltech.edu."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "506542c0-bb40-4f34-979d-b274715314b6",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import necessary modules and packages\n",
    "from tools import model_str, GWD  # Import custom modules from the tools package\n",
    "import random  # Import the random module for generating random numbers\n",
    "import numpy as np  # Import the numpy package for numerical operations\n",
    "import matplotlib.pyplot as plt  # Import matplotlib for plotting graphs\n",
    "\n",
    "# Import specialized packages for gravitational wave interferometry\n",
    "import finesse  # Import the finesse package for modeling optical interferometers\n",
    "import gwinc  # Import the gwinc package for gravitational wave interferometer noise curves\n",
    "\n",
    "# Import specific classes from the finesse package\n",
    "from finesse.analysis.actions.axes import Noxaxis as Noxaxis  # Import and alias Noxaxis class\n",
    "from finesse.analysis.actions.axes import Xaxis as Xaxis  # Import and alias Xaxis class\n",
    "from finesse.detectors import PowerDetector  # Import the PowerDetector class\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "727e2172-61a2-449e-853f-8c2e9ad25f51",
   "metadata": {},
   "source": [
    "## Cell Block 1 - Get your unique parameters\n",
    "Run this when you are told to."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "37cb7ee5-4649-41a7-8f70-3031121d336b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get your unique detector parameters for today's lesson\n",
    "myGWD = GWD()  # Create an instance of the GWD class\n",
    "myGWD.print_params()  # Print the laser power and length of the GWD instance"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cc78984b-9dcf-4fad-a18e-ce4657ff1c22",
   "metadata": {},
   "source": [
    "## Call Block 2 - Building your model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "68aab452-d105-41b3-b6c2-8dfe90c679ca",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Build a Michelson Interferometer model in Finesse\n",
    "model = myGWD.get_MI()  # Get the Michelson Interferometer model using the method from the GWD instance\n",
    "\n",
    "# Define a function to add power detectors to the interferometer model\n",
    "def add_power_detectors(model_in):\n",
    "    # Add power detectors at specified locations in the model\n",
    "    model_in.add(\n",
    "        (\n",
    "            PowerDetector(f\"Px\", model_in.ETMX.p1.i),  # Power detector at the ETMX mirror\n",
    "            PowerDetector(f\"Py\", model_in.ETMX.p1.i),  # Another power detector at the ETMX mirror\n",
    "            PowerDetector(f\"Pprc\", model_in.PRM.p2.i),  # Power detector at the PRM mirror\n",
    "            PowerDetector(f\"Psrc\", model_in.SRM.p1.i)  # Power detector at the SRM mirror\n",
    "        )\n",
    "    )\n",
    "\n",
    "# Call the function to add power detectors to the Michelson Interferometer model\n",
    "add_power_detectors(model)\n",
    "\n",
    "# Check if the detector build is okay by running the model and printing out expected powers\n",
    "sim = model.run(Noxaxis())  # Run the model with no specific axis (Noxaxis)\n",
    "for detector in sim.detectors:\n",
    "    # Print the name and the measured power in watts for each detector\n",
    "    print(f\"{detector} = {sim[detector]:.1f} W\")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "972d7035-0529-4f6e-ab3f-1095ddd563f7",
   "metadata": {},
   "source": [
    "## Cell Block 3 - A Simple Michelson"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "53e7d259-a89d-40f6-aaff-d2f377b3bf05",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a deep copy of the Michelson Interferometer model\n",
    "model2 = model.deepcopy()\n",
    "\n",
    "# Define a string containing the modulation code\n",
    "modulation_code = \"\"\"\n",
    "# Differentially modulate the arm lengths\n",
    "fsig(1)  # Define a frequency signal (fsig) with a frequency of 1 Hz\n",
    "sgen darmx LX.h  # Generate a differential arm motion signal for the X arm\n",
    "sgen darmy LY.h phase=180  # Generate a differential arm motion signal for the Y arm with a phase shift of 180 degrees\n",
    "\n",
    "# Define a quantum noise detector at SRM's output port, including radiation pressure noise\n",
    "qnoised NSR_with_RP SRM.p2.o nsr=True  \n",
    "\n",
    "# Define a quantum noise detector at SRM's output port, excluding radiation pressure noise\n",
    "qshot NSR_without_RP SRM.p2.o nsr=True  \n",
    "\"\"\"\n",
    "\n",
    "# Parse and add the modulation code to the deep copied model\n",
    "model2.parse(modulation_code)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5da68f28-bf85-460d-a54b-0ff386933ff1",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define the frequency axis for the model simulation\n",
    "# Xaxis(frequency, scale, start, stop, points)\n",
    "ax = Xaxis(model2.darmx.f, 'log', .8, 500e3, 1000)  # Create a logarithmic frequency axis from 0.8 Hz to 500 kHz with 1000 points\n",
    "\n",
    "# Run the simulation on the modified model with the defined frequency axis\n",
    "MI = model2.run(ax)\n",
    "\n",
    "# Plot the noise spectral density (NSR_with_RP) of the simulation results\n",
    "# logy=True and logx=True set logarithmic scales for both axes\n",
    "# separate=False plots all data on the same graph\n",
    "MI.plot('NSR_with_RP', logy=True, logx=True, separate=False)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fb9b0971-9fc4-4465-9f7c-f3d9d8b446bd",
   "metadata": {},
   "source": [
    "## Cell Block 4 - A FPMI"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "68a36550-11bf-41d7-9489-7b8266990d25",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get the Fabry-Perot Michelson Interferometer (FPMI) Finesse model\n",
    "# using the method from the GWD instance\n",
    "model = myGWD.get_FPMI() \n",
    "\n",
    "# Add power detectors to the FPMI model\n",
    "add_power_detectors(model)  # Call the function to add power detectors to the model\n",
    "\n",
    "# Parse and add the modulation code to the FPMI model\n",
    "model.parse(modulation_code)  # Apply the modulation code to the model\n",
    "\n",
    "# Check if the detector build is okay by running the model and printing out expected powers\n",
    "sim = model.run(Noxaxis())  # Evaluate the power in the model at DC frequencies\n",
    "for detector in sim.detectors:\n",
    "    # Only print power measurements for detectors whose names start with 'P'\n",
    "    if detector.startswith('P'):\n",
    "        print(f\"{detector} = {sim[detector]:.1f} W\")  # Print the name and measured power in watts for each power detector\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "97786c2a-0074-4102-8f36-6505db50c887",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Run the FPMI model simulation with the defined frequency axis\n",
    "FPMI = model.run(ax)\n",
    "\n",
    "# Create a figure and axis for plotting\n",
    "fig, ax = plt.subplots()\n",
    "\n",
    "# Plot the noise spectral density (NSR_with_RP) for the Simple Michelson Interferometer\n",
    "# MI.x0 contains the frequency values, and MI['NSR_with_RP'] contains the noise values\n",
    "ax.loglog(MI.x0, MI['NSR_with_RP'], color='r', ls='-', label='Simple Michelson')  # Red solid line\n",
    "\n",
    "# Plot the noise spectral density (NSR_with_RP) for the Fabry-Perot Michelson Interferometer\n",
    "# FPMI.x0 contains the frequency values, and FPMI['NSR_with_RP'] contains the noise values\n",
    "ax.loglog(FPMI.x0, FPMI['NSR_with_RP'], color='b', ls='--', label='Fabry Perot Michelson')  # Blue dashed line\n",
    "\n",
    "# Add a legend to the plot\n",
    "ax.legend()\n",
    "\n",
    "# Set the y-axis label\n",
    "ax.set_ylabel('Quantum Noise [1/sqrt(Hz)]')\n",
    "\n",
    "# Set the x-axis label\n",
    "ax.set_xlabel('Frequency [Hz]')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eecff6c7-2722-4b32-9fe5-737300ee92e5",
   "metadata": {},
   "source": [
    "## Cell Block 5 - Power and Signal Recycling"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "592f10ec-04a5-4e4b-9060-2ff64f74422f",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Build models for PRFPMI and DRFPMI\n",
    "model_pr = myGWD.get_PRFPMI()  # Get the Power-Recycled Fabry-Perot Michelson Interferometer (PRFPMI) model\n",
    "model_dr = myGWD.get_DRFPMI()  # Get the Dual-Recycled Fabry-Perot Michelson Interferometer (DRFPMI) model\n",
    "\n",
    "# Add power detectors to the PRFPMI and DRFPMI models\n",
    "add_power_detectors(model_pr)  # Call the function to add power detectors to the PRFPMI model\n",
    "add_power_detectors(model_dr)  # Call the function to add power detectors to the DRFPMI model\n",
    "\n",
    "# Parse and add the modulation code to both models\n",
    "model_pr.parse(modulation_code)  # Apply the modulation code to the PRFPMI model\n",
    "model_dr.parse(modulation_code)  # Apply the modulation code to the DRFPMI model\n",
    "\n",
    "# Evaluate the models' DC response and print the power detected by each power detector\n",
    "print('='*15+'\\nPRFPMI Powers\\n'+'='*15)\n",
    "sim = model_pr.run(Noxaxis())  # Run the PRFPMI model to evaluate its DC response\n",
    "for detector in sim.detectors:\n",
    "    if detector.startswith('P'):  # Only print power measurements for detectors whose names start with 'P'\n",
    "        print(f\"{detector} = {sim[detector]:.1f} W\")  # Print the name and measured power in watts for each power detector\n",
    "\n",
    "print('='*15+'\\nDRFPMI Powers\\n'+'='*15)\n",
    "sim = model_dr.run(Noxaxis())  # Run the DRFPMI model to evaluate its DC response\n",
    "for detector in sim.detectors:\n",
    "    if detector.startswith('P'):  # Only print power measurements for detectors whose names start with 'P'\n",
    "        print(f\"{detector} = {sim[detector]:.1f} W\")  # Print the name and measured power in watts for each power detector\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f54ddb04-4d40-42e9-a374-c9941b24bbdc",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define frequency axis for PRFPMI model simulation\n",
    "ax = Xaxis(model_pr.darmx.f, 'log', .8, 500e3, 1000)\n",
    "PRFPMI = model_pr.run(ax)  # Run PRFPMI model simulation with defined frequency axis\n",
    "\n",
    "# Define frequency axis for DRFPMI model simulation\n",
    "ax = Xaxis(model_dr.darmx.f, 'log', .8, 500e3, 1000)\n",
    "DRFPMI = model_dr.run(ax)  # Run DRFPMI model simulation with defined frequency axis\n",
    "\n",
    "# Create figure and axis for plotting\n",
    "fig, ax = plt.subplots()\n",
    "\n",
    "# Plot the noise spectral density (NSR_with_RP) for different interferometer configurations\n",
    "ax.loglog(MI.x0, MI['NSR_with_RP'], color='r', ls='-', label='Michelson Interferometer (MI)')  # Red solid line\n",
    "ax.loglog(FPMI.x0, FPMI['NSR_with_RP'], color='b', ls='--', label='Fabry Perot MI (FPMI)')  # Blue dashed line\n",
    "ax.loglog(PRFPMI.x0, PRFPMI['NSR_with_RP'], color='g', ls='--', label='Power Recycled FPMI (PRFPMI)')  # Green dashed line\n",
    "ax.loglog(DRFPMI.x0, DRFPMI['NSR_with_RP'], color='g', ls=':', label='Dual Recycled FPMI (DRFPMI)')  # Green dotted line\n",
    "\n",
    "# Add a legend to the plot\n",
    "ax.legend()\n",
    "\n",
    "# Set the y-axis label\n",
    "ax.set_ylabel('Quantum Noise [1/sqrt(Hz)]')\n",
    "\n",
    "# Set the x-axis label\n",
    "ax.set_xlabel('Frequency [Hz]')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e72e1363-bcd4-4c8f-a449-568b4ddb41e3",
   "metadata": {},
   "source": [
    "## Cell Block 6 - Other Noises"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "56dc3735-8821-4141-a180-7604d17c3a0d",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load the Advanced LIGO noise budget\n",
    "budget = gwinc.load_budget('aLIGO')\n",
    "\n",
    "# Update the noise budget parameters with custom values\n",
    "budget.ifo.update({\n",
    "    'Infrastructure.Length': myGWD.L,  # Update the infrastructure length parameter\n",
    "    'Laser.Power': myGWD.P,  # Update the laser power parameter\n",
    "    'Optics.Curvature.ITM': myGWD.L * 1934 / 4000,  # Rescale the ITM curvature for the new length\n",
    "    'Optics.Curvature.ETM': myGWD.L * 2245 / 4000,  # Rescale the ETM curvature for the new length\n",
    "})\n",
    "\n",
    "# Run the noise budget simulation\n",
    "trace = budget.run()\n",
    "\n",
    "# Plot the noise budget simulation results\n",
    "fig = trace.plot()\n",
    "ax = fig.axes[0]\n",
    "\n",
    "# Set y-axis label for the noise plot\n",
    "ax.set_ylabel('Noise [1/sqrt(Hz)]')\n",
    "\n",
    "# Overlay Finesse simulation results on the noise budget plot\n",
    "ax.loglog(DRFPMI.x0, DRFPMI['NSR_with_RP'], color='k', ls=':', lw=4, label='Finesse Simulation')\n",
    "\n",
    "# Add a legend to the plot\n",
    "ax.legend()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "98cbf73a-c713-4115-9a03-6ed75cfb900a",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
